class ChangeTaskListToHomework < ActiveRecord::Migration[5.0]
  def change
    rename_table  :task_lists, :homeworks
  end
end
