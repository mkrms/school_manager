class AddCourseIdToHomeworks < ActiveRecord::Migration[5.0]
  def change
    add_reference :homeworks, :course, foreign_key: true
  end
end
