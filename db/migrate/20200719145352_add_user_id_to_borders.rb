class AddUserIdToBorders < ActiveRecord::Migration[5.0]
  def change
    add_reference :borders, :user, foreign_key: true
  end
end
