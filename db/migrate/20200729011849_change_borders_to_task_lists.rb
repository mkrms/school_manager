class ChangeBordersToTaskLists < ActiveRecord::Migration[5.0]
  def change
    rename_table :borders, :task_lists
  end
end
