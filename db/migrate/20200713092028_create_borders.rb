class CreateBorders < ActiveRecord::Migration[5.0]
  def change
    create_table :borders do |t|
      t.text :memo
      t.string :subject
      t.date :deadline
      t.string :content

      t.timestamps
    end
  end
end
