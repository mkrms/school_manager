# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Course.create!(:course_name => 'L1')
Course.create!(:course_name => 'L2')
Course.create!(:course_name => 'L3')
Course.create!(:course_name => 'L4')

Course.create!(:course_name => 'M2')
Course.create!(:course_name => 'E2')
Course.create!(:course_name => 'C2')
Course.create!(:course_name => 'Z2')

Course.create!(:course_name => 'M3')
Course.create!(:course_name => 'E3')
Course.create!(:course_name => 'C3')
Course.create!(:course_name => 'Z3')

Course.create!(:course_name => 'M4')
Course.create!(:course_name => 'E4')
Course.create!(:course_name => 'C4')
Course.create!(:course_name => 'Z4')

Course.create!(:course_name => 'M5')
Course.create!(:course_name => 'E5')
Course.create!(:course_name => 'C5')
Course.create!(:course_name => 'Z5')

User.create!(:name => 'r00e00', :email => 'admin@example.com', :course_id => 1, :password => 'admin0000', :admin => 'true')
