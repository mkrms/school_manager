class CommentsController < ApplicationController
    def create
        @homework = Homework.find(params[:homework_id])
        @comment = current_user.comments.new(comment_params)
        @comment.user_id = current_user.id

        if @comment.save!
        redirect_back(fallback_location: root_path)
        else
        redirect_back(fallback_location: root_path)
        end
    end

    def destroy
        @comment = Comment.find(params[:id])
        if @comment.destroy
            redirect_back(fallback_location: root_path)       
        else
            redirect_back(fallback_location: root_path)       
        end
    end

    private
    def comment_params
        params.require(:comment).permit(:comment_content, :homework_id, :user_id) 
    end
end
