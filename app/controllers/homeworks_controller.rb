class HomeworksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_homework, only: [:show, :edit, :update, :destroy]
  def index
    @homework = current_user.course.homeworks.all
  end

  def show
    @comments = @homework.comments
    @comment = current_user.comments.new
  end

  def edit
  end

  def new
    @homework = Homework.new
    @course = current_user.course
  end

  def create
    @homework = Homework.new(homework_params)
    @homework.course_id = current_user.course_id
    @homework.user_id = current_user.id
    if @homework.save!
      redirect_to homeworks_path, notice: '作成できました'
    else
      render :new, alert: '作成できませんでした'
    end
  end

  def update
    if @homework.update(homework_params)
      redirect_to homeworks_path, notice: '更新できました'
    else
      render :show, alert: '更新できませんでした'
    end
  end

  def destroy
    if @homework.destroy
      redirect_to homeworks_path, notice: '削除しました'
    else
      redirect_to homeworks_path, alert: '削除に失敗しました'
    end
  end

  private
    def set_homework
      @homework = Homework.find(params[:id])
    end

    def homework_params
      params.require(:homework).permit(:memo, :subject, :content, :deadline)
    end
end
