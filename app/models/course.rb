class Course < ApplicationRecord
    has_many :users
    has_many :homeworks
    has_many :subjects
end
