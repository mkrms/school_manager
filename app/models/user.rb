class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :tasks, dependent: :destroy
  has_many :homeworks, dependent: :destroy
  has_many :comments, dependent: :destroy
  belongs_to :course
  validates :name, format: {with: /\A[hr]\d{2}[mecz]\d{2}\z/}
end
