Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  root 'homes#index'
  resources :homes do
    collection do
      get 'about'
      get 'howto'
    end
  end
  resources :tasks
  
  resources :homeworks do
    resources :comments, only: [:create, :destroy]
  end

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
